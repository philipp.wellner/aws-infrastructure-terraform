provider "aws" {
  region = "us-east-1"
}

#Create key-pair for logging into EC2 in us-east-1
resource "aws_key_pair" "webserver-key" {
  key_name   = "webserver-key"
  public_key = file("~/.ssh/id_rsa.pub")
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}


resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"

  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    createdBy = "Philipp Wellner"
    project   = "aws-terraform-mhp"
    neededUntil : 2021 - 10 - 31
    Name = "terraform-vpc"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    createdBy = "Philipp Wellner"
    project   = "aws-terraform-mhp"
    neededUntil : 2021 - 10 - 31
    Name = "main"
  }
}

#Get all available AZ's in VPC for master region
data "aws_availability_zones" "azs" {
  state = "available"
}

#Create subnet # 1 in us-east-1
resource "aws_subnet" "subnet" {
  availability_zone = element(data.aws_availability_zones.azs.names, 0)
  vpc_id            = aws_vpc.main.id
  cidr_block        = "10.0.1.0/24"
}

data "aws_route_table" "main_route_table" {
  filter {
    name   = "association.main"
    values = ["true"]
  }
  filter {
    name   = "vpc-id"
    values = [aws_vpc.main.id]
  }
}

#Create route table in us-east-1
resource "aws_default_route_table" "internet_route" {
  default_route_table_id = data.aws_route_table.main_route_table.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
  tags = {
    createdBy = "Philipp Wellner"
    project   = "aws-terraform-mhp"
    neededUntil : 2021 - 10 - 31
    Name = "Terraform-RouteTable"
  }
}

resource "aws_security_group" "sg" {
  name        = "sg"
  description = "Allow 80 and 22 inbound traffic"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "22 from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "80 from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    createdBy = "Philipp Wellner"
    project   = "aws-terraform-mhp"
    neededUntil : 2021 - 10 - 31
    Name = "allow_tls"
  }
}

output "Webserver-Public-IP" {
  value = aws_instance.webserver.public_ip
}
